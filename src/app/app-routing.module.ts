import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AllProductsComponent} from './product/all-products/all-products.component';
import {MainWindowComponent} from './main-window/main-window.component';
import {AllClientsComponent} from './client/all-clients/all-clients.component';
import {AllTransactionsComponent} from './transaction/all-transactions/all-transactions.component';
import {StatisticsComponent} from './statistics/statistics.component';

const routes: Routes = [
  {path: 'products', pathMatch: 'full', component: AllProductsComponent},
  {path: 'clients', pathMatch: 'full', component: AllClientsComponent},
  {path: 'transactions', pathMatch: 'full', component: AllTransactionsComponent},
  {path: 'statistics', pathMatch: 'full', component: StatisticsComponent},
  {path: '', pathMatch: 'full', component: MainWindowComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
