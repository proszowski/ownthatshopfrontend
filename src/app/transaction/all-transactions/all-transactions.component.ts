import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatTableDataSource} from '@angular/material';
import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {TransactionsProviderService} from '../transactions-provider.service';
import {FlatTransaction} from '../flat-transaction';
import {ProductsProviderService} from '../../product/products-provider/products-provider.service';
import {ClientsProviderService} from '../../client/clients-provider/clients-provider.service';
import {FlatTransactionDetails} from '../flat-transaction-details';
import {Client} from '../../client/client';
import {ClientDetailsComponent} from '../../client/all-clients/all-clients.component';
import {AddTransactionComponent} from '../add-transaction/add-transaction.component';
import {TransactionsMapperService} from '../transactions-mapper.service';

@Component({
  selector: 'app-all-transactions',
  templateUrl: './all-transactions.component.html',
  styleUrls: ['./all-transactions.component.css']
})
export class AllTransactionsComponent implements OnInit {
  dataSource = new MatTableDataSource<FlatTransaction>();
  columnsToDisplay = ['Select', 'Id', 'Company', 'FirstName', 'Surname', 'TotalPrice'];
  transactionsFetched = false;
  selectedTransactions = new SelectionModel<FlatTransaction>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private transactionsProvider: TransactionsProviderService,
              private productsProvider: ProductsProviderService,
              private clientsProvider: ClientsProviderService,
              private dialog: MatDialog,
              private transactionMapper: TransactionsMapperService) {

  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.transactionsProvider.getAllTransactions().subscribe(
      result => {
        this.dataSource.data = result.map(t => this.transactionMapper.toFlat(t));
        this.transactionsFetched = true;
      }
    );
  }

  addNewTransaction() {
    const dialogRef = this.dialog.open(AddTransactionComponent, {
      height: '80vh',
      width: '80vw',
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if(result != null){
          this.transactionsProvider.getAllTransactions().subscribe(
            all => this.dataSource.data = all.map(t => this.transactionMapper.toFlat(t))
          )
        }
      }
    )
  }

  delete() {

  }

  applyFilter(value: any) {

  }

  masterToggle() {
    this.isAllSelected() ?
      this.selectedTransactions.clear() :
      this.dataSource.data.forEach(transaction => {
        this.selectedTransactions.select(transaction);
      });
  }

  isAllSelected() {
    const numSelected = this.selectedTransactions.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  showTransactionDetails(transaction: FlatTransaction) {
    const dialogRef = this.dialog.open(TransactionDetailsComponent, {
      data: {transaction},
      height: '80vh',
      width: '80vw',
    });
  }
}

@Component({
  templateUrl: './transaction-details.component.html',
  selector: 'app-transaction-details',
  styleUrls: ['./transaction-details.component.css']
})
export class TransactionDetailsComponent {

  transaction: FlatTransaction;
  editableTransaction: FlatTransaction;
  displayedColumns: string[] = ['Index', 'Name', 'Quantity', 'UnitPrice'];
  detailsDataSource: FlatTransactionDetails[];


  constructor(public dialogRef: MatDialogRef<TransactionDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog) {
    this.transaction = this.data.transaction;
    this.editableTransaction = new FlatTransaction(this.transaction);
    this.detailsDataSource = this.transaction.transactionDetails;
  }

  openClientDetails(client: Client) {
    const dialogRef = this.dialog.open(ClientDetailsComponent, {
      data: {client},
      height: '80vh',
      width: '80vw',
    });
  }
}
