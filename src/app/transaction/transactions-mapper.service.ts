import { Injectable } from '@angular/core';
import {ClientsProviderService} from '../client/clients-provider/clients-provider.service';
import {FlatTransaction} from './flat-transaction';
import {Transaction} from './transaction';
import {TransactionDetailsMapperService} from './transaction-details-mapper.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionsMapperService {

  constructor(private clientsProvider: ClientsProviderService,
              private transactionDetailsMapper: TransactionDetailsMapperService) {
  }

  fromFlat(flatTransaction: FlatTransaction): Transaction {
    const transaction: Transaction = new Transaction();
    transaction.id = flatTransaction.id;
    transaction.clientId = flatTransaction.client.id;
    transaction.totalPrice = flatTransaction.totalPrice;
    transaction.date = flatTransaction.date;
    if (flatTransaction.transactionDetails.length > 0) {
      transaction.transactionDetails = flatTransaction.transactionDetails.map(td => this.transactionDetailsMapper.fromFlat(td));
    } else {
      transaction.transactionDetails = [];
    }
    return transaction;
  }

  toFlat(transaction: Transaction): FlatTransaction {
    const flatTransaction: FlatTransaction = new FlatTransaction(null);
    flatTransaction.id = transaction.id;
    this.clientsProvider.getClientWithId(transaction.clientId).subscribe(result => flatTransaction.client = result);
    flatTransaction.totalPrice = transaction.totalPrice;
    flatTransaction.date = transaction.date;
    flatTransaction.transactionDetails = transaction.transactionDetails.map(td => this.transactionDetailsMapper.toFlat(td));
    return flatTransaction;
  }
}
