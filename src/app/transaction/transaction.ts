import {TransactionDetails} from './transaction-details';
import {FlatTransaction} from './flat-transaction';

export class Transaction {
  id: number;
  date: string;
  totalPrice: number;
  clientId: number;
  transactionDetails: TransactionDetails[];


  constructor() {
    this.transactionDetails = [];
  }
}
