export class Address {
  id: number;
  street: string;
  city: string;
  country: string;
  postcode: number;
  houseNumber: number;
}
