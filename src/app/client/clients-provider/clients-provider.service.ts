import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Client } from '../client';
import {ClientType} from '../client-type';
import {Company} from '../company';

@Injectable({
  providedIn: 'root'
})
export class ClientsProviderService {

  private base = 'https://localhost:5001/api/clients';

  constructor(private http: HttpClient) {
  }

  getAllClients(): Observable<Client[]> {
    return this.http.get<Client[]>(`${this.base}`);
  }

  updateClient(client: Client) {
    return this.http.put<Client>(`${this.base}/update`, client);
  }

  deleteClients(clientId: number) {
    this.http.delete(`${this.base}/delete/${clientId}`).subscribe();
  }

  addNewClient(client: Client) {
    return this.http.post<Client>(`${this.base}/add`, client);
  }

  getAllClientTypes(){
    return this.http.get<ClientType[]>(`${this.base}/types`);
  }

  getAllCompanies() {
    return this.http.get<Company[]>(`${this.base}/companies`);
  }

  getClientWithId(clientId: number) {
    return this.http.get<Client>(`${this.base}/${clientId}`);
  }
}
