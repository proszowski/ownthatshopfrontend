import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FiltersDto} from './filters-dto';
import {QueryDto} from './query-dto';

@Injectable({
  providedIn: 'root'
})
export class StatisticsProviderService {

  private base = 'https://localhost:5001/api/statistics';

  constructor(private http: HttpClient) { }

  getResult(result: string, queryDto: QueryDto) {
    return this.http.post(`${this.base}/${result}`, queryDto);
  }
}
