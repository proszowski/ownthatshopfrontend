import {Filters} from './filters';

export class Query {
  filters: Filters;

  constructor() {
    this.filters = new Filters();
  }
}
