#Own That Shop

##Opis

Jest to prosty program (adminka) do obsługi sklepu. Poniższy screen menu dobrze wyjaśnia przeznaczenie tego projektu:

![Alt text](/screenshots/menu.png?raw=true "Menu")

Jesteśmy w stanie zarządzać klientami, produktami oraz transakcjami. Mamy również zakładkę statystyki, w której jesteśmy w stanie zobaczyć dane przefiltrowane w różny sposób 

###Klienci

![Alt text](/screenshots/clients.png?raw=true "Klienci")
- możliwość dodania nowego klienta  
- możliwość usunięcia klienta  
- możliwość modyfikacji klienta  
- możliwość przeglądania danych klientów  
###Produkty
Analogincznie jak dla klientów, CRUD dla produktów:
![Alt text](/screenshots/products.png?raw=true "Produkty")

###Transakcje
![Alt text](/screenshots/transactions.png?raw=true "Statystyki")
- możliwość zobaczenia transakcji i jej szczegółów  
- możliwość dodania transakcji  


###Statystyki
![Alt text](/screenshots/statistics.png?raw=true "Statystyki")
- możliwość przeglądania danych z użycie różnych filtrów
Dostępne filtry:
	- cena produktu (min/max)  
	- data zakupu  
	- dla konkretnego klienta  
	- typ klienta  
 	- dla konkretnego produktu  

##Architektura
REST API - po stronie którego jest cała logika aplikacji (np. wydobywanie danych do statystyk czy walidacja encji) serwuje dane klientom. Główne założenie jest takie, że po stronie klienta dane są tylko i wyłącznie wyświetlane. [Link do repozytoria front-endu 
](https://bitbucket.org/proszowski/ownthatshopfrontend/src/master/) 

###Schemat bazy danych:
![Alt text](/screenshots/schema.png?raw=true "schema")

###Endpointy:

GET /api/products  
GET /api/products/{productId}  
GET /api/products/types  
PUT /api/products/update  
DELETE /api/products/delete/{productId}  
POST /api/products/add  

GET /api/transactions  
POST /api/transactions/check-availability/{productId}  
POST /api/transactions/add  

GET /api/clients  
GET /api/clients/{clientId}  
PUT /api/clients/update  
DELETE /api/clients/delete/{id}  
POST /api/clients/add  
GET /api/clients/types  
GET /api/clients/companies  

POST /api/statistics/{resultType}  

##Technologie
Do backendu użyłem ASP.NET z językiem C#. Jako baza danych posłużył mi PostgreSQL, postawiony na zdalnym serwerze Heroku.  
Connection String:  
OWN_THAT_SHOP_DB_CONNECTION=User ID=\*\*\*\*\*;Password=\*\*\*\*\*\*\*;Server=ec2-54-247-96-169.eu-west-1.compute.amazonaws.com;Port=5432;Database=\*\*\*\*;Integrated Security=true;SSL Mode=Require; Trust Server Certificate=True  

Dane do połączenia schowane z powodów bezpieczeństwa.  

Za narzędzie do tworzenia front-endu posłużył mi Angular.  
