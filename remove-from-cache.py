import subprocess
with open('.gitignore', 'r') as ignored:
    for line in ignored:
        if "#" not in line:
            print('git rm --cached' + line.replace('\n','*;'))
